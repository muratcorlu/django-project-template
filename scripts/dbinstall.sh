#!/bin/sh

createdb -E UTF-8 {{ project_name }}
python manage.py syncdb
python manage.py migrate
