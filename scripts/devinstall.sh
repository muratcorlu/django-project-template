#!/bin/sh
source `which virtualenvwrapper.sh`

mkvirtualenv --distribute {{ project_name }}
$VIRTUAL_ENV/bin/pip install -r $PWD/requirements/dev.txt

echo "export DJANGO_SETTINGS_MODULE={{ project_name }}.settings.dev" >> $VIRTUAL_ENV/bin/postactivate
echo "unset DJANGO_SETTINGS_MODULE" >> $VIRTUAL_ENV/bin/postdeactivate

deactivate
workon {{ project_name }}