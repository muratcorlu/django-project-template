from {{ project_name }}.settings.base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'reporter@muratcorlu.com'
EMAIL_HOST_PASSWORD = 'DKx2wJA15ix8L3G'
EMAIL_PORT = 587
EMAIL_SUBJECT_PREFIX = '[{{ project_name }}] '
EMAIL_BACKEND = 'mailer.backend.DbBackend'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

COMPRESS_ENABLED = True

SESSION_COOKIE_SECURE = True

SESSION_COOKIE_HTTPONLY = True

ALLOWED_HOSTS = ()